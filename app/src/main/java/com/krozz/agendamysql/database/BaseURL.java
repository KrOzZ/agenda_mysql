package com.krozz.agendamysql.database;

public class BaseURL {
    //private static final String baseURl = "http://192.168.0.11/agendaphp-master/";
    private static final String baseURl = "https://vivamazatlantours.com/Practicas/agendaphp-master/";

    public static final String URL_REGISTRAR = baseURl + "wsRegistro.php";
    public static final String URL_ACUTALIZAR = baseURl + "wsActualizar.php";
    public static final String URL_CONSULTAR_TODOS = baseURl + "wsConsultarTodos.php";
    public static final String URL_ELIMINAR = baseURl + "wsEliminar.php";
}
